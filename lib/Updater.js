var AWS = require('aws-sdk');
var fs = require('fs');
var s3 = new AWS.S3();

var  util = require("./Util");

var exports = module.exports = {};


var EventEmitter = require("events");
var Util = require('util');


var Updater = new EventEmitter();

var filterDownloadedFiles = function(files){
    console.log("filterDownloadedFiles", files);

    fs.readdir("./downloads",
                    function(error, alreadyDownloadedFiles){
                     var filesToDownload = [];
    
                        if(error){
                            console.log(error);
                        }
                        else{
                           
                           alreadyDownloadedFiles.sort();
                           alreadyDownloadedFiles.forEach(function(file){
                                var indexOfFileToDownload = files.download.indexOf(file);

                                if(indexOfFileToDownload !== -1){
                                    files.download.splice(indexOfFileToDownload,1);
                                }
                           });
                          
                           if(files.download.length > 0){
                             Updater.emit("update:begin", files);
                           }
                           else{
                             Updater.emit("update:complete", files);
                           }
                        }
   });
    
};
var latestListRetrieved = function(latestFiles){
  
    util.getCurrentMediaList(function(err, current){
        var currentMediaList = [];

        if(err){
            console.log(err);
        }
        else {
       
            current.music.forEach(function(playlistItem){
                var mp3File = "";
                var vtt = "";

                if(playlistItem.hasOwnProperty("file")){
                   mp3File = playlistItem.file.replace("media/music/","");
                   currentMediaList.push(mp3File);
                }
                if(playlistItem.hasOwnProperty("vtt")){
                  vtt = playlistItem.vtt.replace("media/music/","").replace(".vtt",".cue");
                  currentMediaList.push(vtt);
                }
               
            });
            current.video.adverts.forEach(function(advert){
                var advertFile = advert.replace("media/adverts/","");
                currentMediaList.push(advertFile);
            });
            currentMediaList.sort();  
        }

       //Check what files we already have
       console.log("Current Media List: ", currentMediaList);
       var filesWeAlreadyHave = []
       var filesForRemoval = [];
       
       currentMediaList.forEach(function(file){
           var indexOfCurrentFile = latestFiles.latest.indexOf(file);
           if(indexOfCurrentFile === -1 ){
                filesForRemoval.push(file);
            }
           else{
                filesWeAlreadyHave.push(file);
            }
       });
 
       latestFiles.latest.forEach(function(file){
            var indexOfFileToBeRemoved = filesForRemoval.indexOf(file);
            if(indexOfFileToBeRemoved !== -1 ){
                filesForRemoval.splice(indexOfFileToBeRemoved,1);
            }
       });
        
       filesWeAlreadyHave.forEach(function(file){
            var indexOfLatestFile = latestFiles.latest.indexOf(file);
        
            if(indexOfLatestFile !== -1){
                latestFiles.latest.splice(indexOfLatestFile,1);
            }
       });
       
       if(latestFiles.latest.length > 0) {
            filterDownloadedFiles({ "download" : latestFiles.latest, "remove" : filesForRemoval,
                                    "remoteFiles": latestFiles.remoteFiles,
                                    "alreadyCopied": filesWeAlreadyHave,
                                    "remoteContents": latestFiles.remoteContents});
       }
       else{
            Updater.emit("update:uptodate");
       }
          
    });
};
var deletePartialDownloads = function(files){
    console.log("deletePartialDownloads");

    files.forEach(function(file){
  
      if(file.Key !== "media/"){
       try{
            var stats = 
                fs.statSync("/home/odroid/gymix/downloads/"+file.Key);

            if(stats && stats.size !== file.Size){
                try{
                    fs.unlinkSync("/home/odroid/gymix/downloads/"+file.Key);
                }catch(UnlinkError){
                    console.log(UnlinkError);
                }
           }
        }catch(Error){ }
      }
      
    });

};

Updater.checkForUpdates = function(){
    var downloader = s3.listObjects({
        Bucket: "gymix",
        Delimiter: "/",
        Prefix: "media/"
    }, function( err, data){
        var latestFiles = [];
        var onServer = [];

        if( err ){ 
            console.log(err);
            Updater.emit("update:error", err);
        }
        else{
            var latestFiles = [];
            if(data && data.hasOwnProperty("Contents")){
                var contents = data.Contents;

                contents.forEach( function(file){
                    if(file.hasOwnProperty("Key") && file.Key !== "media/"){
                        file.Key = file.Key.replace("media/","");
                        latestFiles.push(file.Key);
                    }
                });
                deletePartialDownloads(data.Contents);
                
                if(latestFiles.length > 0){
                    latestFiles.sort();

                    latestFiles.forEach(function(file){
                        onServer.push(file);
                    });
                    latestListRetrieved({ remoteFiles : onServer, latest: latestFiles,
                                          remoteContents: data.Contents});
                }
            }
        }
    });
};

module.exports = Updater;
