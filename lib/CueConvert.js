"use strict";

var cueSheetParser = require("cue-parser-plus");
var sys = require('sys')

var exec = require('child_process').spawnSync;
var htmlencode = require('htmlencode');

var exports = module.exports = {};

var convertToWebVVT = function(parsedObject){
    console.log("CueConvert.convertToWebVVT()");
    var webVVT = "WEBVTT\n\n";

    parsedObject.tracks.forEach(function(track){
       webVVT += track.number + "\n" + track.start + " --> " + track.end + "\n";
       if(track.performer == "" && track.title == ""){
            webVVT += "Muscle Music\n\n";
       }
       else{
        webVVT += track.performer + " - " + track.title +"\n\n";
       }
    });

    
    return webVVT;
    
};
var calculateLastTrackEndTime = function(parsedObject){
    console.log("CueConvert.calculateLastTrackEndTime()"+parsedObject);
   /* var command = 'mp3info -p "%S" ' + parsedObject.file;
    var options = { cwd : "./downloads" };
    var mp3Length = exec("/usr/bin/mp3info",["-p",'"%S"', parsedObject.file], options);
    var seconds = mp3Length.stdout.toString();
    seconds = seconds.replace('"','');
    seconds = seconds.replace('"','');
    console.log("Seconds"+seconds);

    var hours = Math.floor(seconds / 3600);
    seconds %= 3600;
    var minutes = Math.floor(seconds / 60);
    seconds %= 60;
    
    if(hours < 10){
        hours = "0".concat(hours);
    }

    if(minutes < 10){
        minutes = "0".concat(minutes);
    }

    if(seconds < 10){
        seconds = "0".concat(seconds);
    }
*/
    var hours="06";
    var minutes="00";
    var seconds="00";
   //get the last track
   var lastTrack = parsedObject.tracks.length;
   
   if(lastTrack > 0){
        lastTrack--;
        parsedObject.tracks[lastTrack].end = hours + ":" + minutes + ":"
                                             + seconds + ".000";
   }
};
var approximateEndTimes = function( parsedObject){
    console.log("CueConvert.approximateEndTimes()");

    var idx = parsedObject.tracks.length - 1;
    var hour = parsedObject.tracks[idx].hour;
    var min = parsedObject.tracks[idx].min;
    var sec = parsedObject.tracks[idx].sec; 
    var ms = parsedObject.tracks[idx].ms;
    var tmp = parsedObject.tracks[idx];
    
    idx--;
    var currentTrack = {};
    for(; idx >=0; idx--){
        var endString = "";
        currentTrack = parsedObject.tracks[idx];
        ms = 500;
        if( sec > 0 ){
            sec--;
            
            if( sec < 10){
                sec = "0".concat(sec);
            }
        }
        else{
            sec = "59";

            min--;
          
            if(min < 0){ min = 0;}

            if(min < 10){
                min = "0".concat(min);
            }
           
            hour--;
            if(hour < 0){ hour= 0;}

            if(hour < 10){
                hour = "0".concat(hour);
            }
           
        }
        endString += hour + ":" + min + ":" + sec +"." + ms;
        parsedObject.tracks[idx].end = endString;

        hour = currentTrack.hour;
        sec = currentTrack.sec;
        min = currentTrack.min;
        ms = currentTrack.ms;

    }
};

exports.convertCueSheet = function(cueFile){
    console.log("CueConvert.toWebVVT() ", cueFile);
    var cueSheet = {};
    var parsedObject = {};

    try{
       
        cueSheet = cueSheetParser.parse(cueFile);
        console.log("convertCueSheet() got here");
 //       console.log(cueSheet.files[0].tracks);
        parsedObject.performer = htmlencode.htmlEncode(cueSheet.files[0].performer) || "";
        parsedObject.title = htmlencode.htmlEncode(cueSheet.files[0].title) || "";
        parsedObject.file = cueSheet.files[0].name;
        parsedObject.type = cueSheet.files[0].type.toLowerCase();
        parsedObject.tracks = []; 

        cueSheet.files[0].tracks.forEach(function(track){
        	console.log(track.number+" "+ track.title+" "+ track.performer+" "+track.indexes);
            var currentTrack  = {
                hour: "00",
                min : "00",
                sec : "00",
                ms  : "000",
                start: "00:00:00.000",
                end: "00:00:00.000",
                number : track.number,
                title : track.title,
                performer : track.performer,
//                start : "00:00:00.000",
  //              end: "00:00:00.000"
            };

            track.indexes.forEach(function(index){
            
                var tmpTime = 0;
                if(index.time.min > 59){
                    tmpTime = Math.floor(index.time.min / 60);
  
                    if(tmpTime < 10){
                        currentTrack.hour = "0".concat(tmpTime);
                    }
                    else {
                        currentTrack.hour = tmpTime;
                        console.log(currentTrack.hour);
                    }

                    tmpTime = index.time.min % 60;

                   
                }
                else{
                    tmpTime = index.time.min;
                }
                
                if(tmpTime < 10){
                    currentTrack.min = "0".concat(tmpTime);
                } 
                else{
                    currentTrack.min = tmpTime;
                }
            
                if(index.time.sec < 10){
                    currentTrack.sec = "0".concat(index.time.sec);
                }
                else{
                    currentTrack.sec = index.time.sec;
                }

              currentTrack.start = currentTrack.hour + ":" + currentTrack.min +
                                    ":" + currentTrack.sec + "." + 
                                    currentTrack.ms;
              console.log(currentTrack.start );
           });
            parsedObject.tracks.push(currentTrack);
        });

                 
       approximateEndTimes(parsedObject);
       calculateLastTrackEndTime(parsedObject);
       return convertToWebVVT(parsedObject);
      
       
    }catch(Error){
        console.log("Error parsing cue sheet "+Error, cueFile);
    }

};
